## Tsinghua 
Tsinghua serves internet primarily through its WiFi networks. Connect to either Tsinghua or Tsinghua 5G and then visit net.tsinghua.edu.cn and login. If your device was bought in the last five years you should be able to connect to Tsinghua-5G, it is the faster of the two networks in terms of WiFi speed. This makes little difference for general web browsing and a big difference when doing file transfers. 

You have 25GB free per month after which you will need to load money. 

## Topping-up internet data

There are several methods to top-up internet data to your Tsinghua account.

### Account Settings website

1.  Go to [Account Settings](https://usereg.tsinghua.edu.cn/login.php)
2.  Login in the Tsinghua account you want to top-up
3.  In the home page it is possible to verify the current credit value and to select the top-up option<br>
<img src="../img/usereg-topup.png" width="50%" height="50%"/>
4.  Insert the amount of credit wanted to top-up in your account<br>
<img src="../img/usereg-topup-value.png" width="50%" height="50%"/>
5.  Select the payment method: WeChat or Alipay
6.  After the payment is complete it will take a few minutes until the credit is update to your account


### DIVI
This a special network found at most non dormitory buildings. The very first time you connect you'll have to provide details for your Tsinghua email. Afterwards you won't be asked. This network is free and allows some access to western websites that are normally not accessible. Speeds here vary greatly depending on shared usage and sometimes you won't even be able to reliably connect. But hey free internet right?

### Other networks
We don't yet know how to access them. Let us know if you know and we'll add updates. 

## IPv6
You may have heard of IPv6 but aren't entirely sure what it is. Think of it as a parallel highway right on top of the one you've already been using which is named IPv4. Why you need to know about is twofold. The first is that at Tsinghua using IPv6 is free. Secondly, if you use Google's DNS servers (a free service that helps your computer or phone find websites) you can visit a handle of your favourite sites for free. Yes, some of the biggest websites in the world, the ones you're missing.

Note however that when you route through IPv6 and Google's DNS servers some China only websites won't work.

### Google DNS
Google's DNS server addresses are
2001:4860:4860::8888
2001:4860:4860::8844

#### Apple OSX
##### Quick
System preferences 
Network
Advanced 
DNS tab 
Remove existing servers and add the Google DNS servers. 

##### Preferred 
Same as above but first create an alternative location. Afterwards you can quickly change locations by clicking the Apple in the top left of your screen. This is very useful if you have different settings for different locations. 

#### Microsoft Windows 10

#### iOS
Connect to an IPv6 enabled WiFi network. 
Tap the "i" button in the WiFi settings menu. 
Scroll down to DNS and select manual. 
Add the Google DNS servers. 

#### Android


### Where do I get access
Tsinghua and Tsinghua-5G in the dorms, and DIVI elsewhere usually. 

It really depends on the device providing the WiFi. You can always test by trying to visit a website before logging to net.tsinghua.edu.cn. 


## Western websites
Want to access some of those websites you were used to at home? There are few ways.

The easiest way that will work everywhere is using as service that provides access. There are lots of free ones but we haven't found one that always works and is fast. If you need to read your gmail and post your instagrams like us then you'll want to consider paying. We recommend [Express](https://thuguide.gitlab.io/ThuGuide/redirects/express_referral.htm), follow our link and you can try it for [30 days free](https://thuguide.gitlab.io/ThuGuide/redirects/express_referral.htm).

## Remote access
There are some pages of the Tsinghua Portal that are unaccessible outside the university network.

If you are having a beer in a hutong, getting some waves in Indonesia or in chillin' in your mom's sofa, you gonna need to use an application called Pulse Secure to have full access to the Tsinghua's websites. You can download the version that better suits you on the links below.

**Desktop:**<br>
[Windows 32 bit](http://info.tsinghua.edu.cn/out/download/ps-pulse-win-5.1r5.1-b61437-32bitinstaller.zip)<br>
[Windows 64 bit](http://info.tsinghua.edu.cn/out/download/ps-pulse-win-5.1r5.1-b61437-64bitinstaller.rar) <br>
[OSX](http://info.tsinghua.edu.cn/out/download/PulseSecure5.2.5.pkg)

**Mobile:**<br>
[Android](https://play.google.com/store/apps/details?id=net.pulsesecure.pulsesecure)<br>
[iOS](https://itunes.apple.com/cn/app/pulse-secure/id945832041?mt=8)

After downloading and installing the Pulse Secure, you just to create a new connection with the configurations below, save and connect to it. After connecting succesfully a new screen will appear where you just have to fill your password and done! You have full access to the Tsinghua's websites just like in campus.

**URL:** https://sslvpn.tsinghua.edu.cn <br>
**Username:** *Your student Id - Example:* 2018666666

<img src="../img/Screenshot_SSLVPN.png" width="50%" height="50%"/>
