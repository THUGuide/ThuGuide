# Places

## International Student Dorms

Zijing 18 ([AMaps](http://f.amap.com/2cxth_09A5bFe))<br>
Washing and Dry Cleaning service on ground floor

Zijing 19 ([AMaps](http://f.amap.com/rBPs_09859Js))<br>
 Registration for your room on level 1. There's also an ATM there.

Zijing 20 ([AMaps](http://f.amap.com/13kKS_0665a2T))<br>
No special functions.

Zijing 21 ([AMaps](http://f.amap.com/4ZJE4_0245aIE))<br>
No special functions.

Zijing 22 ([AMaps](http://f.amap.com/3fexN_09C59Vu))<br>
Level 1 is where you will register your visa and register for your degree or non degree program. You will be required to pay cash for your [visa](visa) process but there is no ATM in this building. 

Zijing 23 ([AMaps](http://f.amap.com/4Fud6_0B85ach))<br>
No special functions.

## Most useful gates 

清华大学东北们 North East Gate ([AMaps](http://f.amap.com/31Bhz_03A3BUT))  
清华大学北门 North Gate [AMaps](http://f.amap.com/uBrk_0E35bWd)  
清华大学东南门 South East Gate ([AMaps](http://f.amap.com/5NLI9_0DF39zc))  
清华大学西门 West Gate 1 ([AMaps](http://f.amap.com/5c9cu_0495af7))

## On Campus
**李兆基科技大楼 Lee Shao Kee Building** ([Amaps](http://f.amap.com/58ntB_0465a0u))  
**舜德楼 Shunde Building** （[Amaps](https://ditu.amap.com/place/B000AA0WVO)）  
**紫荆操场 C Building Playground** ([AMaps](http://f.amap.com/58ntB_0465a0u))  
**C楼 C Building** ([AMaps](http://f.amap.com/5EJLZ_0785boT))  
**京东商城货物自提点 Package Pickup Spot** ([AMaps](http://f.amap.com/44wzz_0F059Kr))  

## Off Campus
中国联通 China Unicom Near the South East Gate ([AMaps](http://f.amap.com/6D4BK_0DD58FG))  
北京国际旅行卫生中心 Beijing International Travel Health Center (Haidian Branch) [AMaps](http://f.amap.com/yhcY_0C15bMd)

## Canteens
The food at Tsinghua is just ridiculous. SO so so very good. There are lots of canteens to explore with dishes from all over China and even some western dishes. Come with an open mind and an empty belly and you will not be disappointed. 

If you have favourite dishes, do let us know. We may add it and you to the list!

|Name|Opening Hours|Location|Payment|
|:---------------------|:---------------------------------------:|:-------------------------------------------------------:|:----:|
|观畴园 Guānchóuyuán|06:30 - 9:00, 11:00 - 13:00, 17:00 - 19:00| [View in Maps](https://ditu.amap.com/place/B000A7P667) | Purple Card, Green Card |
|桃李园  Táolǐyuán|06:30 - 9:00, 11:00 - 13:00, 17:00 - 22:30| [View in Maps](https://ditu.amap.com/place/B0FFFYK9KR) | Purple Card, Green Card |
|紫荆园 Zǐjīngyuán|06:30 - 9:00, 11:00 - 13:00, 17:00 - 19:00| [View in Maps](https://ditu.amap.com/place/B0FFF4NS7W) | Purple Card | 
|听涛园 Tīngtāoyuán|06:30 - 9:00, 11:00 - 13:00, 17:00 - 19:00| [View in Maps](https://ditu.amap.com/place/B000A80T1V) |Purple Card |
|丁香园 Dīngxiāngyuán|06:30 - 9:00, 11:00 - 13:00, 17:00 - 19:00| [View in Maps](https://ditu.amap.com/place/B0FFFALVO0) |Purple Card |
|清芬园 Qīngfēnyuán|06:30 - 9:00, 11:00 - 13:00, 17:00 - 19:00| [View in Maps](https://ditu.amap.com/place/B0FFFFK4EO) |Purple Card |
|玉树园 Yùshùyuán |06:40 - 8:30, 11:00 - 13:30, 17:00 - 20:30| [View in Maps](https://ditu.amap.com/place/B000A81JO9) | Purple Card, Green Card |
|芝兰园 Zhīlányuán |11:00 - 13:00, 17:00 - 20:00 | [View in Maps](https://ditu.amap.com/place/B000AA57FG) |Purple Card |
|荷园 Héyuán|11:00 - 13:00, 17:00 - 19:00 | [View in Maps](https://ditu.amap.com/place/B000A81ISQ) |Purple Card|
| 清真餐厅 Muslim Canteen  |10:30 - 22:30 | [View in Maps](https://ditu.amap.com/place/B0FFFFJM0E) | Purple Card, Green Card |
| 清青快餐 Qīng qīng Fast Food |10:30 - 22:30 | [View in Maps](https://ditu.amap.com/place/B0FFFFK4EO) | Purple Card, Green Card, Cash |
| 清青比萨 Qīng qīng pizza  | 10:30 - 22:00| [View in Maps](https://ditu.amap.com/place/B0FFF4NS7W) | Purple Card, Green Card, Cash |
| 清青永和 Qīng qīng yǒnghé |06:30 - 14:00, 16:30 - 21:30 | [View in Maps](https://ditu.amap.com/place/B000A7P667) | Purple Card, Green Card |
| 清青休闲餐厅 Qīng qīng xiūxián cāntīng  | 06:30 - 14:00, 16:30 - 21:30 | [View in Maps](https://ditu.amap.com/place/B0FFFYK9KR) | Purple Card, Green Card |

## 听涛园 Tīngtāoyuán
Layout: Single floor canteen with two sections, some of the best noodles on campus are found here.  
Best dish: 油泼扯面Yóupōchěmiàn (noodles splashed with a touch of hot oil) with some 腊汁肉Làzhīròu (pulled pork). You can find it in the second, smaller section of the canteen tucked around the back.  
Recommender: Cadu  

<img src='../img/Canteen_YouPoCheMian.jpeg' width="50%" height="50%"/>

<img src='../img/Meal_YouPoCheMian.jpeg' width="50%" height="50%"/>