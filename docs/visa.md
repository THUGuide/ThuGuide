# Visa

## Extending Your Visa

**Location:** Zijing 22 ([AMaps](http://f.amap.com/3fexN_09C59Vu)), 1st floor, room 100.

**Times:** Mon – Fri, 13:00 – 15:30

**Items required:** 

*  Visa application permit (A little piece you get at the next desk in the same room)
*  Passport
*  Registration Form of Temporary Residence<sup>1</sup>
*  550 RMB in cash for the regular service or 650 for the express service :money_with_wings: 
*  1 Passport sized photo

**Photo requirements:**

*  Two inches
*  White background
*  Full face
*  Showing your ears
*  No stain

**Types of service:** The regular service takes around 3 weeks and the express service around 2 weeks. In any case just make sure to ask specifically for the date you will be able to collect your passport.

**Steps:**

**1.**  After gathering all the needed items go to Zijing 22 ([AMaps](http://f.amap.com/3fexN_09C59Vu)), 1st floor, room 100. You should have everything except the Visa application permit. This is a little piece of paper that you get in the same room and literally transport from one desk to the next. It's just to show that you are supposed to have your visa extended until a certain date.

**2.**  Go the desk at the back of the room to get a form stating that all your payments to the university are ok - If you are covered by the CSC Scholarship it will be, don't worry. Say you want to extend your visa and they will ask for your Student Id, check your information and then they will give your Visa Application permit.

**3.**  After getting the Visa Application permit go to the desk at the entrance of the room with the permit and all the required items. Give the *Visa Guy* all the items and say the type of service you want: regular or express. He will collect everything plus the amount in cash according with the type of service you chose (550 for regular and 650 for express).

 
**WARNING:** If you want/need to travel during the time your passport is taken there is an "Yellow Paper"that you can use instead of the passport to take trains, flights inside China and doing Hotel check in. In this case, make sure to ask it for the *Visa Guy* before handing him the items, so he can prepare one for you.


**4.**  Having processed everything you will receive a Visa Receipt with the date you should collect you passport.

* Visa Receipt (due date July 11th)
<img src="../img/Certificate_Visa-Recept.jpg" width="80%" height="80%"/>

Now you're done. To collect your passport just go to the same place carrying your Visa Receipt in the due date or after and will you be able to show off your brand new Resident Permit.

**Appendix**

^**1** This is how the Registration Form of Temporary Residence looks like:
<img src="../img/Certificate_Registration-Form-of-Temporary-Residence.jpg" width="80%" height="80%"/><br>

In case your residence permit was eaten by your dog or you just cannot find it, you can go to the first floor of Zijing 19 ([AMaps](http://f.amap.com/rBPs_09859Js)) and request a new one. They will print it and give you right away.