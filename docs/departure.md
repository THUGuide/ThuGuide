# Departure

## Bus routes to the airport

### Zhongguancun Line

The Airport Shuttle Bus route is extended to TusPark, one way trip ticket fee is of RMB 30, which should be paid directly to the driver in cash. Bus station located at the north of Sohu Media Building, opposite to Ziguang Mansion. Updated details are shown below:

#### From TusPark to BCIA
First bus at 5:10 ; last bus at 22:00. Buses depart every 30 minutes.）

```mermaid
graph TD
A[TUSPARK] -->B[Bafusi Bridge]
B --> C[Beihang University - North gate]
C --> D[Huixin West Street]
D --> E[Terminals 2, 1 and 3]
```