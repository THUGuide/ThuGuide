# Pollution

## Understand
If you’re going to spend more than a couple days in Beijing it’s important to understand how the air pollution works.

The first thing you should do is download an app that informs you of the AQI levels. We recommend Air Matters in our [Apps](apps) because it’s accurate and detailed and gives updates for changes during the day.

The AQI is the air quality index and is a measure of a few different particles. The number you need to most watch is related to the PM2.5. These are particles that are small enough to enter your bloodstream and over time do long term damage. 

Ok so these are just numbers, what is high and what is low? Basically an AQI above 50 is high, meaning the air is bad. But you’ll quickly notice the average day in Beijing is 150. Don’t panic but do take precautions. 

What matters is exposure. Exercising and breathing heavily is worse for you when the air quality is bad than simply sitting around when the air quality is bad. 

You can protect yourself from the pollution using a mask and an air purifier. It’s important to note that none of the buildings you will likely have access to are airtight. Which means the air outside your room and inside your room is the same AQI level. Be conscious of sleeping through bad pollution as you may as well be outside. Unless you plan on sleeping with a mask on, you should purchase a mask **and** a purifier. In both cases filters or disposable masks will neee to be replaced. 

## Air Purifiers
Air purifiers are devices that circulate the air in your room through a filter that removes the dangerous particles from the air. In a small dorm room a powerful fan will take a few minutes to bring the level down below 50. Most students opt for a Xiaomi air purifier which not only does the job but looks cool although it also costs 800RMB brand new. Look for secondhand ones and purchase a new filter (~150RMB) which should last a semester. 

All the above said not all air purifiers are created equal but you needn’t buy a fancy one. Any fan blowing air through HEPA filter will clean the air in your room. As such you can pick something more affordable. Just don’t go cheap on the filter, it’s massively important. The fan only needs to push air through the filter. 

## Masks
The crazy thing about masks is that even surgical masks are better than breathing without one but only just barely. You need to get proper masks that do the job. 

### Fit
The most important aspect that is often neglected is to find a mask that fits well. If your expensive or cool mask doesn’t fit your face then you’re just breathing the outside air. Loose fitting masks are a no good. The easy way to test if your mask fits is to put the mask on with the head straps and breath in, if it sticks to your face then it fits. If not then adjust it and if you can’t adjust it sufficiently then look for another mask with a different design. Some masks also have a thin piece of metal around the nose bridge which you can bend into place. This is usually one of the hallmarks of a good mask. 

Getting the right fit if you have a beard  is very challenging, look out for masks that offer a soft rubber lining. 

Head straps, although not as neat looking as ear straps, hold the mask to your face best for most people. Choose headstraps. 

### Rating
The next important aspect is the mask rating. There are many different rating systems but to keep it simple remember 95. Masks with some letters followed by 95 are the minimum you should wear. Wear a higher number and you’re more protected. If your mask has an exit valve then it’s likely a good one. 

It does not matter if you buy disposable masks or reusable ones. The reusable ones tend to be more expensive and need to have their filters changed but their fit can be better which is very important. 

### Brands
The big brand name is 3M. 

### Appearance
Please don’t buy a mask based on it’s appearance, far too many people wear these cool looking grey masks that neither fit well nor offer a good enough rating even if they did. You’re just generating waste for landfills and wasting money. You need to decide if you’re wearing a mask for looks or for your lungs. Please help your friends and fellow students who don’t know the mistakes they’re making.  

## Environment 
Still reading? Whilst I have your attention. There’s a lot we can do to improve the planet. Prevention is better than cure and it’s betrer to reduce before we recycle. No, I’m not going to tell you to stop eating meat but consider eating less and aiming for the higher quality grass fed free range variety. Choose beef last, take public transport and don’t wash your clothes so often. That’s all. 