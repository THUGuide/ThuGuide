# Community

## WeChat Groups
This is how things happen in China. WeChat groups are everything. You'll make ones with your friends just to pay for dinner, or with your TA to get help with homework, and for all the societies that you will join.

Also, almost every building has a building WeChat group and they are very useful. You should try join your building group by asking for an invite from people in the elevator or those you see in the lobby. If you're an old student, let's build the community and invite the new faces.

Groups are capped at **500 members** and you have to be personally invited if the group is larger than 100. Unfortunately they are hard to advertise or publicise so you have to keep lookout for new ones. There are usually lots of secondhand sales groups that extremely valuable to join.

<!-- ## Slack group
WeChat is essential for life in China but we think we can do better for our community at Tsinghua. Slack is an application available on all platforms and is much better suited for large groups, in fact we've created one massive group. Join our new Tsinghua community on Slack: 

[ZijingX](https://zijingx.slack.com/signup)

It's a place to ask questions, find out what is happening around you, make friends, buy and sell items, share the dankest memes, and more.  -->