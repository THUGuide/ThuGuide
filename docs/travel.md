# Travel

Travelling in China is much easier than you'd initially imagine. Extensive high speed railway networks and excellent public transport make it very easy to get around.

To get started you need just three things, your passport, a place to stay and a transport.

# Transport
Bus, plane, and train are primary choices. Trains may seem to take longer on the surface but unlike planes, they are almost never delayed or cancelled, the stations are usually very well located, tickets prices do not change depending on which minute you checked. 

The easiest app to use here is Trip.com, formerly Ctrip. The app is in English and is very user friendly. They do charge a high processing feee but we think it's worth the convenience.

# Accommodation

The best apps and website here are [Airbnb](https://thuguide.gitlab.io/ThuGuide/redirects/airbnb_referral.htm) and [Booking.com](https://thuguide.gitlab.io/ThuGuide/redirects/booking_referral.htm). 

If you're a first time traveller, why not sign up for [Airbnb](https://thuguide.gitlab.io/ThuGuide/redirects/airbnb_referral.htm) and [Booking.com](https://thuguide.gitlab.io/ThuGuide/redirects/booking_referral.htm) you'll receive 170RMB for nothing and we'll get some credit too. 