## Purpose
This [guide](Home) is collection of things we wish we'd known and answers to questions we didn't know to ask. We're a couple of students who want to make your time here as smooth as possible.

## Team
[Gabriel](http://www.linkedin/in/gabrielmpaula) - WeChat:
 [gabrielmpaula](<img src='../img/Logo_China-Unicom2.png' width='30%' height='30%'>)<br>
[Tiisetso](http://tiisetso.com)

## Contributors
Special thanks to a few friends who've given us valuable feedback.  
Katja :fi:, Maëlys :fr:, Cadu :br:, Kate :au:.

## Tips
If you like this guide and found it helpful, please send us a tip. We'll use it for :coffee:, :pizza: and keeping this project alive! 

<img src='../img/WeChat_Coffee-Code.png' width="30%" height="30%"/> 
