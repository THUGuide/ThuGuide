# Activities

## Gym

### Comprehensive Gym
**Location:** West entrance ([Maps](https://www.amap.com/place/B000A8W7HK))<br>
**Hours:** 09:00 - 22:00<br>
**Price:** 1000元/year - 200元/month

### Community Gym
**Location:** Opposite side of Library Cafe - Basement Floor ([Maps](https://www.amap.com/regeo?lng=116.3285905122757&lat=40.00472854427086&name=Community Gym))<br>
**Hours:** 09:00 - 22:00<br>
**Price:** Free!

### New Community Gym
**Location:** ([Maps](https://www.amap.com/regeo?lng=116.32810637354852&lat=40.00876561371968&name=New Community Gym))<br>
**Hours:** 09:00 - 22:00<br>
**Price:** Free!

### Outside Gyms
**Locations:** There are several on Campus, some are: Near Zijing Field ([Maps](https://www.amap.com/regeo?lng=116.33061558008194&lat=40.00924429365358&name=Open Gym)), Near Library ([Maps](https://www.amap.com/place/B0FFG2J8C3))<br>
**Hours:** 24 hours<br>
**Price:** Free!

# Live Shows & Events

The Tsinghua Auditorium (清华大学学堂 qīnghuá dàxué xuétáng) has several events going on, like live shows, movies, performances, etc. To check what's going and what's the next events happening you can check their website (only in chinese).

**Website:** http://www.hall.tsinghua.edu.cn<br>
**WeChat Official Account:** qinghuaxuetang

Usually the prices follow the standard of the table below.

| Event        |  Price  |
|:-------------|:-------:|
|Cinema (Every Wednesday）| 20-25元 |
|Orchestra     | 20元    |
|Beijing Opera | 20元    | 