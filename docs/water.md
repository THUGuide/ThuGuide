# Water

## Water Delivery
There is a water delivery service for campus that can be request at the reception desk of every student dorm building. They deliver 18.9L bottles and you can get a free dispenser by buying plans with 30 or more bottles.

The service is in the form of a plan, in which you buy an amount of water tickets in advance and order a new water bottle,using your prepaid tickets, everytime the current one is finished. We recommend buying more than 30 bottles so that you get a water dispenser for free. The free dispenser does not cool water but it can heat it to just below building, more than hot enough for tea or instant noodles. 30 bottles is almost enough for two people across two semesters or one person for 3-4 semesters. At the end of each semester people often sell water tickets and so if you over order you can often sell. 

| Amount | Dipenser type |
|:------:|:-------------:|
|30|Small; Hot and natural water|
|60|Big; Hot and natural water|
|120|Big; Hot and cold water|

To get the service, go to the reception desk of your dorm building and request it. They will show you the available options of brands, types of water, and costs. You will have to say your name, room number, and mobile number then pay in advance. You'll receive a special code that will allow them to identify your address each time along with a stack of water tickets.

Afterwards you can order more water bottles by placing your empty bottle outside your room door with a single water ticket attached then place an order through their WeChat channel or place a phone call. The delivery is same day but a quick tip is to either buy an empty bottle from someone so that you have two and can always have a water available whilst one is being exchanges or just to ask for two bottles at the start. 

## Ordering using WeChat
After placing the empty bottle(s) in the hallway outside your apartment door with the same amount of water tickets under/in/on/attached to it, then follow the steps below:

1.  Search for the Official Account: 清紫源泉 (QīngZǐYuánQuán)
2.  Follow the Official Account
3.  Select the option: 在线订购 (ZàiXiànDìngGòu) - Online order
4.  Fill the following options:
    *  **档案号 (Dǎng'àn hào) - File number:** Your 4 digit identification number located on the paper you received. After filling this option some of your information should be loaded. Make sure to confirm your address under 地址 (DìZhǐ)
    * **订水量 (DìngShuǐLiàng) - Quantity:** Select the amount of bottles you want to order. You must have the same number of empty bottles to exchange to new ones.
5. Finally submit your order by clicking in 提交 (TíJiāo) - Submit order.  
There is no need to fill any other information.

**Example**  

<img src="/uploads/e074d1bbc4baccb5f72735b6896cf199/Water_Wechat.png" width="50%" height="50%"/>

Keywords 
**档案号:** 4-digit id  
**订水量:** Amount of bottles  
**地址:** Address

