# Tips

#### I broke my phone screen where do I get it fixed?
There are many options here but here are three immediate ones. 
##### C Building
There is a repair shop at the basement on the same level as the supermaket. It's not necesssarily the cheapest option but it's on campus and fast.
##### Frank
WeChat ID:
```
hapencool
```

Zhongguancun is Beijing's silicon valley and there are hundreds of people who can fix your phone, computer, pretty much anything electronic. Frank is one of them and he speaks English. He is also great for buying secondhand phones. His office is in 1821C 18th floor of the Beijing Centergate Como Co which can be found by going through exit C1 at the Zhongguancun station on line 4.

You can ask for directions, get quotes, and see some of his 99% new phone collection.
##### HiWeiXiu
WeChat ID:
```
hiweixiu
```
This is a company of repair agents that will come right to your doorstep and repair your phone. They offer great prices and repair pretty quickly. The whole service runs through WeChat. It's this kind of service that makes China feel like magic. Look up your phone, select the part you want fixed, and book an appointment. Expect a phone call in Chinese so you might need to get a friend or the Fuwuyuan to help you. They'll come to your building, and will need a desk to work (use the study room!). They are professional and fast.

They also have a website [hiweixiu.com](http://hiweixiu.com) but use their WeChat account. 

#### How can I watch TV on campus?
[iptv.tsinghua.edu.cn](http://iptv.tsinghua.edu.cn) is streaming site from Tsinghua that allows you to stream quite a few chinese channels. Was very useful during the FIFA World Cup! 

#### How often do the cleaning staff come?
Three times a week, no earlier than 08:00 but sometimes precisely at 08:00. They do work on sundays and public holidays. Bed sheets are changed once a week. They follow a regular schedule that is different for every other floor so you can figure it out after a week. 

#### When do visitors have to leave my dorm room?
23:00. The fuwuyuans will call and even knock on your door. 

#### What is my Chinese address?
##### Zijing
```
Name Surname  
中国北京海淀区清华大学紫荆学生公寓24号楼1234A室  
100084
```
Replace 24 with your Zijing building 

Chinese addresses go from big to small. Country first, room last. 

北京(Beijing)市(city)海淀(Haidian)区(district)清华(Tsinghua)大学(University)紫荆(Zijing)学生(student)公寓(Block of flats)24号楼(building number)1234A室（room)

Your family can send post to you just by writing your address in English. Yes, it will arrive. 

Name Surname  
Zijing flats building 24 room 1234  
Tsinghua University  
Haidian District  
Beijing  
China  
100084