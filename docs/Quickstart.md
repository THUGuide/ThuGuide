# Quickstart
The Goal of this page is to get you up and running fast with as little back and forth as possible. We're constantly refining this based on feedback, if you have some please send it our way.

## Day Zero
So you've just landed. What next?

### Get Money
If you don't already have some you'll need at least 530RMB to get from the airport to lying down in your bed on campus with a SIM card in your phone and toilet paper in your toilet. Don't be afraid, China is a lot less expensive than this, it's just day zero.

### Get to Campus 

Follow our note at [Arrival](Arrival) to see details. If you have a lot of bags or arrive late take the [taxi](arrival/#taxi). But if you're feeling adventurous and want to save some money take the airport [bus](arrival/#bus). It will involve more walking, but we think it's the best because it perfectly positions you for the next step. 

### Get a SIM Card

I know right. You're tired and you have bags. This will save you time later and get you internet connected sooner. Go to China Unicom (not China Mobile) and pick a plan. This process is not very quick but getting a SIM card means internet and you won't be able to access campus wifi until you register. See our advice [here](arrival/SIM-card).

If you came by bus then there is a convenient China Unicom on your way. Came by subway? Walk a little a further to the same spot. Came by taxi? Worry about the SIM card later.

### Get a Room

Staying on Campus? Keep reading. Now you need to get to [Zijing Building 19](http://f.amap.com/rBPs_09859Js) and head to level 1. If you came by taxi it's a short walk, but by bus or train it's a long one, just over a kilometre or 3/4 of a mile. You will need to put down a 200RMB deposit in cash when you get there and there is an ATM on the same floor. 

Take your key and documents to your appropriate building. If you're in a Zijing building 20-23, you're probably walked past your building already, otherwise 18 is just behind. Asian Youth Centre in Building 6? Another walk.

Go drop off your bags. 

### Get Supplies

You need a water right? Or maybe some toilet paper. Tsinghua is amazing, there are several supermarkets on campus. Head to [C Building](http://f.amap.com/5EJLZ_0785boT) and go into the basement and be amazed. You can pay cash and there are ATMs near both entrances on the ground floor. 

### Get Something to Eat

If you don't pick something you can get some canteen food. Tsinghua has some amazing food on offer, a few canteens will let you pay cash. Check our [list](Places/#Cateens) and see what's available to you 

### SIM Card
If you don't already have your SIM card it's time to get one now, follow the guide [here](Arrival/#SIM-Card)

## Day One

### Beijing International Travel Health Center
Physical examination time: 08:30 - 11:00  
Location: [AMaps](http://f.amap.com/yhcY_0C15bMd)

If you're on an X1 visa you should know that you need to visit the health centre to have your records verified or have the tests done. Get this done quickly soon after you arrive and you'll hopefully beat the crowds. And if nothing else, get there early to beat the queue.

You can get to the [Health Centre](http://f.amap.com/yhcY_0C15bMd) by taxi or subway but the best way is by bus. Leave through Tsinghua's [North Gate](http://f.amap.com/uBrk_0E35bWd) and catch Bus 365 from this [bus stop](http://f.amap.com/2wea5_0A55byH). The bus is the cheapest, involves not terribly much walking, and gets you the close to the right place.

Once you've completed the process you'll be given a date to come and pick up your certificate. Yes, you'll come to this place at least twice. Pick up happens in the afternoon and is very quick and easy.

### Registration, Bank card, and Visa

These are the next most important steps and will vary depending on your program and when you arrived. This is what you need to know. There is a big holiday coming in the first week of October called Golden week. Your visa will mean you won't have your passport for a while so you'll want to hand that in quickly, but do that too quickly and you won't be able to pick up your bank card when it is ready. So get through your registration where you'll also register for bank card amongst other things. Wait for the bank to call you about your bank card, pick it up, and immediately hand in your passport for the [visa](visa) process, make sure you have enough cash, 550 RMB, and all the necessary documents. If the due date is longer than you're willing to wait you can ask for special 'yellow paper' that you can travel on or pay for the express service, but if you've done everything correctly until now you might not have to.

Once you have your bank it's time to setup your WeChat and Alipay accounts. This won't happen on Day Two but you're not far away and that's when your life will take off in China. 





