# Apps
Here are some of the most useful apps for your time in China.

iOS and OSX apps can be downloaded from the Apple App Store. Once you have your Chinese bank details you can also set up an account for the Chinese region where the apps are often slightly cheaper.

## Computer apps

**Net.tsinghua**, auto login software for your computer named after the tsinghua internet login portal. [OSX/Win10/Linux](http://net-tsinghua.herokuapp.com/) read more [here](https://github.com/ThomasLee969/net.tsinghua)

## Phone apps

**微信 WeChat** ([iOS](https://itunes.apple.com/cn/app/wechat/id414478124?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.tencent.mm&ref=search))
For instant messaging and payments. The importance of this app cannot be understated. 
Equivalent to WhatsApp + PayPal + Facebook + Instagram 

**淘宝 TaoBao**([iOS](https://itunes.apple.com/cn/app/手机淘宝-海外代购掌上助手/id387682726?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.taobao.taobao&ref=search))
Four words, Online Shopping + in China. It's even more incredible than you could imagine. Shipped all the way to university within a few days. For expensive items such as mobiles, computers or original items TaoBao is not recommended. Instead, try using JD.com and TMall due their process of seller verification.

**美团 MeiTuan** ([iOS](https://itunes.apple.com/cn/app/美团-旅行打车订酒店机票必备软件/id423084029?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.sankuai.meituan&ref=search))
Platform with several services: Food delivery, Cinema tickets, Hotel booking, train tickets etc.

**京东 JD.com** ([iOS](https://itunes.apple.com/cn/app/京东-挑好物-上京东/id414245413?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.jingdong.app.mall&ref=search))
Online shopping as well but with verified sellers. More expensive than Taobao usually but when you're buying electronics you're safer with JD.com.

**高德地图 AMaps** ([iOS](https://itunes.apple.com/cn/app/高德地图-精准地图-导航出行必备/id461703208?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.autonavi.minimap&ref=search))
One of the best maps apps inside China.

**饿了么 Ele.me** ([iOS](https://itunes.apple.com/cn/app/饿了么外卖-配送早餐下午茶夜宵/id507161324?l=en&mt=8)/[Android](http://app.mi.com/details?id=me.ele&ref=search))
Similar to Meituan, you can order food from a vast selection of restaurants and have them delivered to your building's lobby.

**支付宝 Alipay** ([iOS](https://itunes.apple.com/cn/app/alipay-simplify-your-life/id333206289?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.eg.android.AlipayGphone&ref=search))
The first payment app. You'll pay more with WeChat but Alipay is necessary for using Taobao. It helps to have both apps for the are occasion only one is accepted. 

**大众点评 Dàzhòng diǎnpíng** ([iOS](https://itunes.apple.com/cn/app/大众点评-分享你的潮生活/id351091731?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.dianping.v1&ref=search))
A app with user ratings for everything. Particularly useful for finding places to eat, from the high end to the hyper local. If you're anywhere in China and looking for a bite to eat this app will help you finding something amazing. Can also book movie tickets, and order food to your doorstep, and loads more.
Western equivalent: Yelp + Zomato + Foursquare + Google Maps

**滴滴出行 DiDi** ([iOS](https://itunes.apple.com/cn/app/didi-greater-china/id554499054?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.sdu.didi.psnger&ref=search))
Chinese Uber, order a wide variety of taxis. A little infamous for having drivers struggle to locate you. Stand in very clear places and make sure your location pin matches where you are. You can also schedule taxi's in advance.

**百度翻译 Baidu Translate** ([iOS](https://itunes.apple.com/cn/app/百度翻译-旅游学习必备多语种翻译词典/id605670941?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.baidu.baidutranslate&ref=search))
Google translate isn't blocked in China but Baidu translate might be better. Not for translation quality but rather for ease of use. Especially on iPhone, you can share highlighted text directly to the translation app.

**天猫 TMall** ([iOS](https://itunes.apple.com/cn/app/天猫商城-中国掌上购物app/id518966501?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.tmall.wireless&ref=search))
Online shopping from official brand stores. It's essentially Taobao but with reliable sellers.

**Pleco** ([iOS](https://itunes.apple.com/cn/app/pleco-chinese-dictionary/id341922306?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.pleco.chinesesystem&ref=search))
Your Mandarin learning companion. Great dictionary with add-ons that are worth buying if you're serious about learning Mandarin. 

**Trip.com**, formerly Ctrip ([iOS](https://itunes.apple.com/cn/app/trip-com-flights-hotels/id681752345?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.ctrip.izuche&ref=search))
Great app for booking flights and train tickets in China. Has a higher booking fee than other apps but is super easy to use. Until you're confident with your chinese skills we recommend it.

**Airbnb** ([iOS](https://itunes.apple.com/cn/app/airbnb/id401626263?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.airbnb.android&ref=search))
Can lead to some really amazing places to stay. Usually on the pricier side.

**Booking.com** ([iOS](https://itunes.apple.com/cn/app/booking-com-travel-deals/id367003839?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.booking&ref=search))
Very comprehensive listing in almost every city, great for hostels and well located spots. 

**Mobike** ([iOS](https://itunes.apple.com/cn/app/mobike-smart-bike-sharing/id1044535426?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.mobike.mobikeapp&ref=search))
The more expensive but reliable of the two big share bike companies. Mobike are the awesome silver and orange bikes.

**Ofo** ([iOS](https://itunes.apple.com/cn/app/ofo共享单车-智能无桩好骑/id1056015676?l=en&mt=8)/[Android](http://app.mi.com/details?id=so.ofo.labofo&ref=search))
The cheaper and more commonplace of the two big share bike companies. The bicycles can be a little unreliable at times, make sure to check the quality of the bike before you ride.

**Netease Music** ([iOS](https://itunes.apple.com/cn/app/网易云音乐-音乐的力量/id590338362?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.netease.cloudmusic&ref=search))
Magical Chinese music app. Can stream pretty much anything for free, and for a small fee store the songs. Equivalent to Spotify + Apple Music + Deezer but possibly better.

**闲鱼 Xiányú**([iOS](https://itunes.apple.com/cn/app/闲鱼-二手回收交易网/id510909506?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.taobao.idlefish&ref=search))
Second hand sales store. Buyer beware but you could find a sweet deal.

**AtTsinghua** ([iOS](https://itunes.apple.com/cn/app/attsinghua/id543499370?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.attsinghua.main&ref=search))
Comprehensive and essential Tsinghua app. Download and explore.

**Tsinghua Now** ([iOS](https://itunes.apple.com/cn/app/tsinghua-now-即刻清华/id1087608650?l=en&mt=8)/[Android]())
Tsinghua apps that helps you manage your homework assignments and see your grade point average. 

**CampNet** ([iOS](https://itunes.apple.com/cn/app/campnet/id1263284287?l=en&mt=8))
This application will auto login in your account to the tsinghua wifi networks. This is a huge timesaver.

**Air Matters** ([iOS](https://itunes.apple.com/cn/app/air-matters/id477700080?l=en&mt=8)/[Android](http://app.mi.com/details?id=com.freshideas.airindex&ref=search))
China is making massive progress on reducing pollution but bad days still creep in. This app is detailed and sends alerts. 