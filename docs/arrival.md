#Arrival

## Getting to Tsinghua
The transport you choose to come into campus depends on you. If you have a lot of bags then taking a taxi is really your only option as the bus or train, although much cheaper, will require much more walking. If you arrive late at night, then you are also limited to taking a taxi. Bus is the cheapest but can be difficult to find the right departure station and finally there's the train. It's the easiest to find and navigate but involves the most walking overall. 

Most direct: Taxi 
Cheapest option: Bus  
Easiest to find when at the airport: Train

In all cases you need to make your way to [Zijing building 19](http://f.amap.com/rBPs_09859Js) where you can register for your room. 

It's worth noting, that almost no one will speak English but everyone will help you out, be patient. There is one notable exception, at the airport there are some people who might try to encourage you to take an unnecessarily expensive taxi, try to avoid to this outcome.

## Taxi
Approximate cost: 120RMB  
Navigate to: Tsinghua University North East Gate 清华大学东北们  
Operating hours: 24/7

Taxi's are expensive but you might be tired and just want to get in as fast as possible.
To catch a taxi head to the basement floor of the airport and only get inside a car with a huge yellow stripe along the sides.

Ignore any people with sashes speaking English offering to help you catch a taxi. Trust us, ignore them. A taxi from the airport to Tsinghua North East Gate should cost about 120RMB. Anything considerably higher you should refuse, you're being hustled.

## Bus 
Approximate cost: 30RMB  
Navigate to: TUSPark (Tsinghua University Science Park)  
Operating hours: 6:50-24:00 Every 30 minutes. Buses depart when fully seated.

This is possibly the best way. It is second most direct for the least money, but it is a tricky to find the right spot to catch the bus. After you get off the bus you'll still need to walk for about kilometre or 3/4 mile or catch a second local bus (549). However, if you catch the airport transfer bus you're in the perfect spot to buy a SIM card.

How to catch the bus
Line 5: Zhongguancun 
The Airport Shuttle Bus route travels from BCIA (Beijing Capital International Airport) to TUSPark which is located just south of Tsinghua University Main Gate. The one way trip ticket fee is 30RMB. Depending on your terminal it can be tricky to find the correct bus departure point. Don't give up, if you see where the buses leave from you're in the right spot.

Here are the ticket office locations at each terminal.
T1: Gate No. 7 (inside) on level 1.
T2: Gate No.11 (outside) on level 1.
T3: Gate No.7 or No.9 (Outside) on level 1.

```mermaid
graph TD
A[Terminals 3, 2 and 1] -->B[Wanghe Bridge]
B --> C[Xiaoying]
C --> D["Asian Games Village (Anhui Bridge)"]
D --> E[Xueyuan Bridge]
E --> F[North of Baofusi Bridge]
F --> G[TusPark]
```

## Train
Approximate cost: 55RMB (20RMB (Subway card fee) + 25RMB (airport transfer) + 10RMB (Subway and Bus to campus)  
Navigate to: Wudaokou Station on the line 13  
Operating hours: T2: 06:35 - 23:10, T3: 06:20 - 22:50, train departs every 10 minutes.

Catching the train is the second cheapest option but will require navigating the subway with your bags along with two line changes and a long walk. The main reason for catching the train is that it's easy to find at the airport, unlike the bus, and there is no chance of being tricked, unlike the taxi. The subway system in Beijing is very easy to navigate even if you don't know any Chinese.   

Take the airport transfer line to Sanyuanqiao, transfer to line 10 until Zhichunlu, then line 13 one stop until Wudaokou.

If you get on the Airport Express train after 22:00 you might find that one of the transfer lines has stopped operating when you arrive. Don't worry, just exit the station and catch a taxi or consider opting for the bus or taxi routes from the airport instead.

Leave through Exit B. The bus for bus stop for bus 549 is 200m down the road. There are a few bus stops nearby so make sure you are at the right one. Once you board the bus it should initially head back towards the subway station before turning left at the intersection. You can use your subway card to pay or 2RMB (the bus driver cannot give change).

## SIM card
Don't buy at the airport, it's overpriced there with limited options. If you take a taxi into campus, buy the SIM card after you registered for your room. If you take the bus or train you should consider getting it before entering campus.

## China Unicom
There are three major cellular service providers in China but you should choose this one. It's most likely to work with your phone and give good signal everywhere. Their competition is China Mobile who have a very convenient store on Campus but is the lesser service provider.

### Finding a store
There are satellite China Unicom stores everywhere but are not easy to spot when you first arrive. The China Unicom logo looks like this   
<img src='../img/Logo_China-Unicom1.png'width='50%' height='50%'>

But the sign you want to look out for in real life looks like this  
<img src='../img/Logo_China-Unicom2.png' width='30%' height='30%'>

We recommend going to this [store](http://f.amap.com/6D4BK_0DD58FG) near the Tsinghua's [South East Gate](http://f.amap.com/5NLI9_0DF39zc). Why? It's a dedicated store and the staff are friendly. They don't speak a word of English but you can still get a SIM card here. 

## Buying a SIM card
This is a special challenge because of language. If you're lucky they will call a special phone number where you can speak to someone who speaks English. However the process is a little convoluted and knowing a little bit about how it works will make it much easier.

You can navigate this process by writing down on paper the specific numbers you'd like. Explain that you'll be in China for a year, or however long, as they have special plans if you're just here for a year. Also specify the amount of GB you estimate you will need and they will find you an appropriate package. We say GB because saying "GB" is very clear that you're talking about gigabytes of data.

It is also possible to get a SIM card that only works in Beijing. If you want to go outside of Beijing, i.e visit another city, you'll be able to receive calls and texts but will need to buy specific data packages if you want data. This is very easy to do with WeChat. 

Everything will be quoted in months. The number they show you is what you'll pay each month to keep your service running. For example there is currently a plan that will give you 20GB of data every month for 99RMB. That seems like it means 99RMB in total but it's actually every month. We'd quote plans here but they are constantly changing and you might even be asked to pay some money upfront if pick a serious plan.

At some point you'll be shown screen with lots of phone numbers. In China some numbers are luckier than others so one with a lot of 8's is more lucky. In some cases a phone number with more 8's will be more expensive. 

You won't be able to pay with your international credit or debit card. Have cash ready. 200RMB is probably the very ceiling most would spend but you can get connected for half of that depending on your needs.
