This is the unofficial Tsinghua wiki, THU Guide. 

Get going right away by starting with our [Quickstart Guide](Quickstart)

If you're [fresh off the plane](arrival) :airplane_arriving: or are about to [head back home](departure) :airplane_departure: we have the answers to the questions you've been meaning to ask and even the ones you didn't know you could. We're talking bus schedules, how not to get ripped off by a black taxi, and which gate to go to when you first arrive. It's the [North East gate](Places), you're welcome. 

Never travelled before? First time in Beijing or at Tsinghua? Here's what we wish we'd known in our [first few days](Quickstart) literally from the moment you arrive on Chinese soil until you say 再见:wave:. Don't get a SIM card at the airport, hand in your passport quickly for the residence permit so you can travel in the first week-long holiday in October, we've got you.

Oh and that convoluted [visa](visa) process finally has some clarity, no more back and forth trips whether you're starting out or renewing. 

Your cellphone is your lifeline here, take a look at the best [apps](apps) to get you going.

Off campus and need to get onto the university network? We've got that [covered](Remote-access) too.

Found this helpful? Send us a [coffee](About/#tips). Something to add? [Let us know](About) We're typing as fast as procrastinating from our masters theses will allow. 

